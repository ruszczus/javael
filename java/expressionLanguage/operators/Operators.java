package expressionLanguage.operators;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name = "operators", eager = true)
@SessionScoped
public class Operators {

	private String operatorEquals = "Operator equals works";

	private String userName = "Iron Man";
	
	public String getOperatorEquals() {
		return operatorEquals;
	}

	public String getUserName() {
		return userName;
	}

	public void setOperatorEquals(String operatorEquals) {
		this.operatorEquals = operatorEquals;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	
}