package expressionLanguage.collections;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

@ManagedBean(name = "library", eager = true)
@RequestScoped
public class Library {
	
	private List<Book> books = new ArrayList<>();

	public Library() {
		addBooks();
	}

	public List<Book> getBooks() {
		return books;
	}

	private void addBooks() {
		books.add(new Book("Rainbow Rowell", "Fiction", 1));
		books.add(new Book("Big little lies", "Fiction", 2));
		books.add(new Book("Mr Mercedes", "Thriller", 3));
		books.add(new Book("Long way home", "Thriller", 4));
		books.add(new Book("The book of life", "Fantasy", 5));
		books.add(new Book("Skin game", "Fantasy", 6));
		books.add(new Book("The Martian", "Sience Fiction", 7));
		books.add(new Book("Lock in", "Sience Fiction", 8));
		books.add(new Book("Prince Lestat", "Horror", 9));
		books.add(new Book("The city", "Horror", 10));
		books.add(new Book("Yes please", "Humour", 11));
		books.add(new Book("Bridget Jones", "Humour", 12));
		books.add(new Book("Diuna", "Fiction", 13));
		books.add(new Book("Harry Potter", "Fiction", 14));
		books.add(new Book("The Witcher", "Fiction", 15));
	}
}
