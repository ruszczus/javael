package expressionLanguage.collections;

public class Book {

	private String name;
	private String type;
	private Integer number;
	
	
	public Book(String name, String type, Integer number) {
		super();
		this.number = number;
		this.name = name;
		this.type = type;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public Integer getNumber() {
		return number;
	}

	public String getName() {
		return name;
	}
	
	public String getType() {
		return type;
	}
	
}
