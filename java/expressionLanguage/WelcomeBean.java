package expressionLanguage;

import javax.faces.bean.ManagedBean;

@ManagedBean(name = "welcome", eager = true)
public class WelcomeBean {
	
	private String message = "I'm alive!";
	
    public WelcomeBean() {
        System.out.println("WelcomeBean instantiated");
    }
    
    public String getMessage() {
        return message;
    }
}
