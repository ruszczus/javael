package expressionLanguage.lambdas;

import java.util.ArrayList;
import java.util.List;

public class Dealer {

    private final String name;
    private final List<Car> cars = new ArrayList<>();

    public Dealer(String name) {
        this.name = name;
    }

	public String getName() {
        return name;
    }

    public List<Car> getCars() {
        return cars;
    }
}