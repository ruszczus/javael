package expressionLanguage.lambdas;


import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

@ManagedBean(name = "demoBean", eager = true)
@RequestScoped
public class DemoBean {

    private final Dealer dealer = new Dealer("NoName");

    public DemoBean() {
        initCars();
    }

    public Dealer getDealer() {
        return dealer;
    }

    private void initCars() {
        dealer.getCars().add(new Car("VW", "red"));
        dealer.getCars().add(new Car("Ford", "green"));
        dealer.getCars().add(new Car("Chevrolet", "red"));
        dealer.getCars().add(new Car("BMW", "blue"));
        dealer.getCars().add(new Car("Ferrari", "red"));
        dealer.getCars().add(new Car("Mercedes", "green"));
        dealer.getCars().add(new Car("Mercedes", "blue"));
    }
}